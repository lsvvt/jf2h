from Bio.PDB import *
from scipy.spatial import cKDTree
import numpy as np
from scipy.spatial import ConvexHull
import sys
import os

nproccs = 1

"""
CONSTANTS
"""
not_lig_residues = ["HOH", "CL", "PO4", "SO4", "GOL", "MES", "PEG", "ACT"]
metals = ["ZN", "CA", "NA", "HG", "MG", "NAP", "NAD", "FAD", "HEM", "H4B", "ACT"]

models = {
"4CWP" :  0, # -1  # -1  #    #
"4CWR" :  0, # -1  # -1  #    #
"4N8Q" : -1, #  2  #  1  #  !  #
"4OWV" : -1, #  2  #  1  #  !  #
"2WEJ" :  0, #  1  #  1  #  !  #
"2WEO" :  0, #  1  #  1  #  !  #
"2XC5" :  0, # -2  # -2  #  -  #
"2XBX" :  0, # -2  # -2  #  -  #
"1C9D" : -2, # -4  # -4  #  d  #
"1C8V" : -2, # -4  # -4  #  d  #
"4B4M" :  0, #  1  #  0  #  -  #
"4ASJ" :  0, #  1  #  0  #  -  #
"5F62" :  1, #  0  #  1  #R !  #
"5F60" :  1, #  0  #  1  #R !  #
"5ZC5" :  0, #  1  # -1  #  !  #
"5ZA9" :  0, #  1  # -1  #  !  #
"6B1C" :  0, #  1  #  1  #  !  #
"6B1K" :  0, #  1  #  1  #  !  #
"5F61" :  1, #  2  #  1  #R !  #
"5F5Z" :  1, #  2  #  1  #R !  #
"2WEG" :  0, #  1  #  1  #  !  #
"1G4O" :  0, #  1  #  2  #  !  #
"1G46" :  0, #  1  #  2  #  !  #
"3F1A" :  0, #  1  #  1  #  !  # !
"3F19" :  0, #  1  #  1  #  !  # !
"1G45" :  0, #  1  #  -  #  -  #
"1G48" :  0, #  1  #R -  #  -  #
"1G52" :  0, #  1  #R -  #  -  #
"1G1D" :  0, #  1  #  -  #  -  #
"1G53" :  0, #  1  #R -  #  -  #
"2BXV" : -1, #  0  #  -  #  -  #
"2FPT" : -1, #  0  #  -  #  -  #
"3KQS" :  1, #  1  #  -  #  -  # !
"3KR2" :  1, #  1  #  -  #  -  # !
"3R16" :  0, #  1  #  -  #  -  #
"3R17" :  0, #  1  #  -  #  -  #
"4N8R" : -1, #  0  #  -  #  -  #
"4N5G" : -1, #  0  #  -  #  -  #
"5EN4" :  0, #  1  #  -  #  -  #
"5L7W" :  0, #  1  #  -  #  -  #
"5TZH" :  0, #  0  #  -  #  -  #
"5TZW" :  0, #  0  #  -  #  -  #
"3PCC" : -3, #  1  #  -  #  -  #
"3PCF" : -3, #  1  #  -  #  -  #
"4LBR" : -1, #  0  #  -  #  -  #
"4LAZ" : -1, #  0  #  -  #  -  #
"4LB3" : -1, #  0  #  -  #  -  #
"3NLX" :  3, #  0  #  -  #  -  #
"3NLZ" :  3, #  0  #  -  #  -  #
"3NLY" :  3, #  0  #  -  #  -  #
"3PNE" :  3, #  0  #  -  #  -  #
"3SVP" :  3, #  0  #  -  #  -  #
"5VIH" : -1,
"5DEX" : -1,
"4P8N" : -1,
"4P8K" : -1,
"1I9M" : 0,
"1I9L" : 0,
"1I9P" : 0,
"1I9O" : 0,
}

def struct2array(struct):
    xyz = []
    for i in struct.get_atoms():
        xyz.append(i.get_coord())
    return np.array(xyz)


def struct2array_name(struct):
    xyz = []
    for i in struct.get_atoms():
        xyz.append(i.get_name())
    return np.array(xyz)


def struct2array_element(struct):
    xyz = []
    for i in struct.get_atoms():
        xyz.append(i.element)
    return np.array(xyz)


def find_arange(xyz1, xyz2, r):
    kdt = cKDTree(xyz1)
    idx = kdt.query_ball_point(xyz2, r)
    return list(set([i for j in idx for i in j]))


def select_residues(idx, struct):
    residues = []
    for i, el in enumerate(struct.get_atoms()):
        if i in idx:
            parent = el.get_parent()
            if parent not in residues:
                residues.append(parent)
    return residues


def get_atoms_id(idx, struct):
    atoms = {}
    for i, el in enumerate(struct.get_atoms()):
        if i in idx:
            parent = el.get_parent()
            if parent not in atoms.keys():
                atoms[parent] = [el]
            else:
                atoms[parent].append(el) 
    return atoms


def structs2array(structs):
    xyz = np.array([])
    for i in structs:
        if len(xyz) == 0:
            xyz = struct2array(i)
        else:
            xyz = np.append(xyz, struct2array(i), axis = 0)
    return xyz

def structs2array_name(structs):
    xyz = np.array([])
    for i in structs:
        if len(xyz) == 0:
            xyz = struct2array_name(i)
        else:
            xyz = np.append(xyz, struct2array_name(i), axis = 0)
    return xyz

def structs2array_element(structs):
    xyz = np.array([])
    for i in structs:
        if len(xyz) == 0:
            xyz = struct2array_element(i)
        else:
            xyz = np.append(xyz, struct2array_element(i), axis = 0)
    return xyz


def point_in_hull(point, hull, tolerance=1e-12):
    return all(
        (np.dot(eq[:-1], point) + eq[-1] <= tolerance)
        for eq in hull.equations)


def get_atom_names(residue):
    ans = []
    for atom in residue:
        ans.append(atom.get_name())
    return ans


def calc_charge(residue):
    ch = 0
    name = residue.get_resname().replace(" ", "")
    a_names = get_atom_names(residue)
    if name in ["ARG", "NAD", "NAP"]:
        ch = +1
    elif name in ["FE", "ZN"]:
        ch = +2
    elif name == "ASP" and "OD1" in a_names and "OD2" in a_names:
        ch = -1
    elif name == "GLU" and "OE1" in a_names and "OE2" in a_names:
        ch = -1
    elif name == "CYS" and "HG" not in a_names:
        ch = -1    
    elif name == "LYS" and "HZ3" in a_names:
        ch = +1
    elif name == "HIS" and "HD1" in a_names and "HE2" in a_names:
        ch = +1
    elif name in ["HEM"]:
        ch = -2
    debug([name, "=", ch])
    return ch

def sslurm(name, path, tp, gfn = "1", nproccs = nproccs):
    slurm = open(path + "/" + name, "w")
    slurm.writelines(["#!/bin/sh\n", "#SBATCH --nodes=1\n", "#SBATCH -n " + str(int(nproccs)) +"\n", "#SBATCH --job-name=" + name + "\n"])

    if tp == "xtb":
        slurm.writelines("""export OMP_STACKSIZE=4G
export OMP_NUM_THREADS=%.1i,1
export MKL_NUM_THREADS=%.1i
ulimit -s unlimited

""" % (nproccs, nproccs))
        slurm.write("xtb --input " + name + ".xtb " + name + ".xyz -P " + str(int(nproccs)) + " --gfn " + gfn + " --opt --gbsa water > " + name + ".out")
    slurm.close()

def debug(stri):
    j = 0
    if "--DEBUG" in sys.argv:
        if type(stri) == type(""):
            print(stri)
        else:
            for pstr in stri:
                print(pstr, end=" ")
        print()
  

def norm(xyz):
    xyz = np.array(xyz)
    return xyz / lengh(xyz)


def lengh(xyz):
    return (xyz[0] ** 2 + xyz[1] ** 2 + xyz[2] ** 2) ** 0.5


def dist(xyz1, xyz2):
    return lengh([xyz1[0] - xyz2[0], xyz1[1] - xyz2[1], xyz1[2] - xyz2[2]])


def sxyz(name, path, xyz, xyz_name, cons = [-1]):
    inp = open(path + "/" + name + ".xyz", "w")
    inp.write(str(len(xyz)) + "\n\n")
    ans = []
    for i in range(len(xyz)):
        at_name = xyz_name[i]
        # if "CL" in xyz_name[i]:
        #     at_name = "Cl"
        # if at_name == "Z":
        #     at_name = "Zn"
        if str(i + 1) not in cons and -1 not in cons:
            ans.append(at_name + " " + str(float(xyz[i][0]) + np.random.normal(0, 0.033333333333333333333333333333333, 1)[0]) + " " + str(float(xyz[i][1]) + np.random.normal(0, 0.033333333333333333333333333333333, 1)[0]) + " " + str(float(xyz[i][2]) + np.random.normal(0, 0.033333333333333333333333333333333, 1)[0]) + "\n")
        else:
            ans.append(at_name + " " + str(xyz[i][0]) + " " + str(xyz[i][1]) + " " + str(xyz[i][2]) + "\n")
    inp.writelines(ans)
    inp.close()

def gen_dir(path, name, n = ""):
    path += "/" + name + n

    if os.path.exists(path):
        # debug("Warning Directory " + path + " already exist")
        pass
    else:
        os.makedirs(path)
    return path



def sxtb(name, path, zar, cons = 0):
    gfn = "2"
    inp = open(path + "/" + name + ".xtb", "w")
    inp.writelines(["$chrg " + str(zar) + "\n", "$spin 0\n"])
    if cons != 0:
        inp.writelines(["$fix\n"])
        inp.write("   atoms: " + cons[0])
        for n in cons[1:]:
            inp.write(", " + n)
        inp.write("\n$end\n")
    inp.write("""$opt
    engine=rf
$end""")
    sslurm(name, path, "xtb", gfn)
    inp.close()


def genrun(inl, n):
    inp = open(inl + "/" + n + ".slurm", "w")
    inp.writelines(["#!/bin/sh\n", "#SBATCH --partition=hpc4-3d\n", "#SBATCH --nodes=1\n", "#SBATCH -n 48\n", "#SBATCH --job-name=" + n + "\n"])
    inp.write("python ../runnohup " + n + "\n")
    inp.write("sleep 3h")


act = sys.argv[1]

parser = PDBParser()
structure = parser.get_structure(act, "models/" + act + ".pdb")


chain = structure[0]["A"]
ligand = 0
for residue in chain:
    # print(residue)
    if not is_aa(residue) and residue.get_resname().replace(" ", "") not in not_lig_residues + metals or residue.get_resname().replace(" ", "") == "BE2":# and residue.id[1] not in [2029, 2032, 2034, 2036, 2040, 2069]:
        if act.upper() in ["1I9M", "1I9L", "1I9O"] and residue.id[1] == 666:
            print("skip", residue)
        else:
            print(residue.get_resname().replace(" ", ""), residue)
            ligand = residue

chain_xyz = struct2array(chain)
ligand_xyz = struct2array(ligand)

idx = find_arange(chain_xyz, ligand_xyz, 4)
res = select_residues(idx, chain)

at_id = get_atoms_id(idx, chain)
i_res = []
for residue in chain:
    if residue in res:
        r_names = get_atom_names(at_id[residue])
        # print(r_names, residue)
        if "O" in r_names:
            for i in residue.get_atoms():
                if i.get_name() == "C":
                    i_idx = find_arange(chain_xyz, [i.get_coord()], 1.8)
                    i_res.append(select_residues(i_idx, chain))
        if "H" in r_names:
            for i in residue.get_atoms():
                if i.get_name() == "N":
                    i_idx = find_arange(chain_xyz, [i.get_coord()], 1.8)
                    i_res.append(select_residues(i_idx, chain))
if len(i_res) != 0:
    for s_res in i_res:
        for ss_res in s_res:
            if ss_res not in res:
                debug(["add OH", ss_res])
                res.append(ss_res)


dele = []
for residue in res:
    debug(["add", residue])
    if residue.get_resname().replace(" ", "") in not_lig_residues:
        dele.append(residue)
for residue in dele:
    debug(["del", residue])
    res.remove(residue)

xyz = structs2array(res)

hull = ConvexHull(xyz)

in_hull = []
idx_hull = []
for i, point in enumerate(chain_xyz):
    if point_in_hull(point, hull):
        in_hull.append(point)
        idx_hull.append(i)

res_hull = select_residues(idx_hull, chain)

dele = []
for residue in res_hull:
    # if residue.get_resname().replace(" ", "") in not_lig_residues and residue.id[1] not in [2029, 2032, 2034, 2036, 2040, 2069]: # 4CWP 4CWR
    # if residue.get_resname().replace(" ", "") in not_lig_residues and residue.id[1] not in [2388, 2393, 2394, 2395, 2412]: # 3KR2 3KQS
    # if residue.get_resname().replace(" ", "") in not_lig_residues and residue.id[1] not in [1001]: # 3nL* 3Nl*
    if residue.get_resname().replace(" ", "") in not_lig_residues:
        dele.append(residue)
for residue in dele:
    res_hull.remove(residue)

# print(res_hull)

i_res = []
for residue in chain:
    # if residue not in res_hull and residue.id[1] in [458, 1018, 1019]: ### 3Nl
    #     res_hull.append(residue)
    #     debug(["append ", residue])
    # if residue in res_hull and residue.id[1] in [569, 568, 596, 597, 603]: ### 3Nl
    #     res_hull.remove(residue)
    #     debug(["remove ", residue])
    if residue in res_hull and residue.id[1] in [666]:
        res_hull.remove(residue)
        debug(["remove ", residue])
    if residue in res_hull and residue.get_resname() == "PRO":
        for i in residue.get_atoms():
            if i.get_name() == "N":
                    i_idx = find_arange(chain_xyz, [i.get_coord()], 1.8)
                    i_res.append(select_residues(i_idx, chain))
if len(i_res) != 0:
    for s_res in i_res:
        for ss_res in s_res:
            if ss_res not in res_hull:
                debug(["+ add for PRO", ss_res])
                res_hull.append(ss_res)

# for residue in chain:
#     j = residue.id[1]
#     if residue in res_hull and residue.get_resname() == "PRO":
#         # print(residue)
#         if chain[j - 1] not in res_hull:
#             print("+", chain[j - 1])
#             res_hull.append(chain[j - 1])
#         if chain[j + 1] not in res_hull:
#             print("+", chain[j + 1])
#             res_hull.append(chain[j + 1])

cons = []
charges = []
delet_at = []
new_at = []
at_num = 0
xyz = structs2array(res_hull)
kdt_hull = cKDTree(xyz)
xyz_name = structs2array_name(res_hull)
xyz_element = structs2array_element(res_hull)
# print(xyz_name)
for residue in res_hull:
    charges.append(calc_charge(residue))
    # print(residue.get_resname())
    for atom in residue:
        a_name = atom.get_name()
        # print(a_name)
        if a_name in ["CA", "C5D", "C5Q", "CMD", "CMB", "C1V", "S1X"]:
            cons.append(1)
            print("fix", a_name)
        else:
            cons.append(0)
        
        if a_name in ["C", "N"]:
            idx = kdt_hull.query_ball_point(atom.get_coord(), 1.8)
            if len(idx) == 3:
                xyz_kdt = list(xyz_name[idx])
                ca_xyz = xyz[idx[xyz_kdt.index("CA")]]
                x_xyz = xyz[at_num]
                if "N" in xyz_kdt:
                    delet_at.append(at_num)
                    delet_at.append(idx[xyz_kdt.index("H")])
                if "C" in xyz_kdt:
                    delet_at.append(at_num)
                    delet_at.append(idx[xyz_kdt.index("O")])
                new_at.append(norm(x_xyz - ca_xyz) * 1.09 + ca_xyz)
                
        at_num += 1
cons += [1] * len(new_at)
cons = np.array(cons)
new_at = np.array(new_at)

cons = np.delete(cons, delet_at, axis=0)

fcons = []
for i, el in enumerate(cons):
    if el == 1:
        fcons.append(str(i + 1))

xyz_f = np.delete(xyz, delet_at, axis=0)
xyz_name_f = np.delete(xyz_name, delet_at)
xyz_element_f = np.delete(xyz_element, delet_at)
xyz_f = np.append(xyz_f, new_at, axis=0)
xyz_name_f = np.append(xyz_name_f, np.array(["H"] * len(new_at)))
xyz_element_f = np.append(xyz_element_f, np.array(["H"] * len(new_at)))

# print(len(xyz_f))
# print()
# for i in range(len(xyz_f)):
#     print(xyz_name_f[i][0], xyz_f[i][0], xyz_f[i][1], xyz_f[i][2])


fcharge = sum(charges) + models[act.upper()]

print("charge", fcharge)
# print(charges)

# sxyz(act, sys.argv[2], xyz_f, xyz_name_f)

path = sys.argv[2] + "/" + act

pathAll = gen_dir(path, act)
sxyz(act, pathAll, xyz_f, xyz_element_f)
sxtb(act, pathAll, fcharge, cons = fcons)

pathLig = gen_dir(path, ligand.get_resname())
sxyz(ligand.get_resname(), pathLig, ligand_xyz, struct2array_element(ligand))
sxtb(ligand.get_resname(), pathLig, models[act.upper()])

for i in range(100):
    pathAll = gen_dir(path, act, "_" + str(i))
    sxyz(act, pathAll, xyz_f, xyz_element_f, cons = fcons)
    sxtb(act, pathAll, fcharge, cons = fcons)

genrun(path, act)

# class s_residues_hull(Select):
#     def accept_residue(self, residue):
#         if residue in res_hull:
#             return 1
#         else:
#             return 0
# io = PDBIO()
# io.set_structure(chain)
# io.save("stru3.pdb", s_residues_hull())
