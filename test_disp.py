import os
import plotly.figure_factory as ff
import numpy as np
import subprocess
import sys

def go(path, ans = []):
    dirs = os.listdir(path)
    for dir in dirs:
        newpath = path + "/" + dir
        if os.path.isdir(newpath):
            go(newpath)
        else:
            ans.append(newpath)
    return ans


def get_path(fpath):
    ans = ""
    tail = ""
    fpath = fpath[::-1]
    j = 0
    for char in fpath:
        if j == 1:
            ans += char
        if char == "/" and j == 0:
            j = 1
        elif j == 0:
            tail += char
    
    return ans[::-1], tail[::-1]

def find_energy_xtb_out(file_path):
    file = open(file_path, "r")
    lines = file.readlines()
    for line in lines:
        if "TOTAL ENERGY" in line:
            line_p = line.split(" ")
            while "" in line_p:
                line_p.remove("")
            return float(line_p[3])

path = sys.argv[1]

files = go(path)
home = os.getcwd()

models_ene = []
group_labels = [sys.argv[1]]
names = []
name = ""
ref = 0

for f in files:
    if ".out" in f and "slurm" not in f:
        if f[-8] != "/":
            models_ene.append(find_energy_xtb_out(f))
            names.append(f)
            if f[-14] == "/":
                print(f)
                ref = find_energy_xtb_out(f)
                name = f.split("/")[-2]



print(names[models_ene.index(min(models_ene))])
p = get_path(names[models_ene.index(min(models_ene))])
p2 = get_path(p[0])
print(p)
print(p2)
num = p2[1][5:]
print("./gdel " + p2[0] + "/" + p[1][:-4] + " " + num)
process = subprocess.Popen("./gdel " + p2[0] + "/" + p[1][:-4] + " " + num, shell=True)

models_ene = (np.array(models_ene) - ref) * 627

fig = ff.create_distplot([models_ene], group_labels, show_hist=False)
fig.update_layout(title_text=name)

fig.write_html(name + ".html")

# fig.show()